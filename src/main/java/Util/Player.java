package Util;

import org.newdawn.slick.Animation;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

/**
 * Created by benjaminmuldrow on 5/16/16.
 */
public class Player extends BasicPlayer {

    // TODO player attributes
    private int speed;
    private float posX;
    private float posY;
    private float [] position = new float [2];
    private boolean moving = false;
    private Animation activeAnimation;
    private int velocity;

    public Player(int x, int y) {
        this.posX = x;
        this.posY = y;
        try {
            SpriteSheet spriteSheet = new SpriteSheet("res/ball_guy_idle.png", 32, 32);
            Animation animation = new Animation(spriteSheet, 250);
            this.setIdle(animation);
            this.setLeftMove(animation);
            this.setRightMove(animation);
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }

    public Player (int x,
                   int y,
                   Animation idle,
                   Animation right,
                   Animation left
                   )
    {
        this.posX = x;
        this.posY = y;
        this.setIdle(idle);
        this.setRightMove(right);
        this.setLeftMove(left);

    }

    public boolean isMoving() {
        return moving;
    }

    public void setMoving(boolean moving) {
        this.moving = moving;
    }

    public float[] getPosition() {
        return new float[]{posX,posY};
    }

    public Animation getActiveAnimation() {
        if (!isMoving() || velocity == 0) {
            activeAnimation = this.getIdle();
        } else if (velocity > 0) {
            activeAnimation = this.getRightMove();
        } else /*if (velocity < 0)*/ {
            activeAnimation = this.getLeftMove();
        }
        return activeAnimation;
    }

    public int getVelocity() {
        return velocity;
    }

    public void setVelocity(int velocity) {
        this.velocity = velocity;
    }

}
