package Util;

import org.newdawn.slick.Animation;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

/**
 * Created by benjaminmuldrow on 5/16/16.
 */
public class ResourceManager {

    public static Animation getAnimation(String spriteSheetPath, int sizeX, int sizeY, int duration) {
        Animation animation = null;
        try {
            SpriteSheet spriteSheet = new SpriteSheet(spriteSheetPath,sizeX,sizeY);
            animation = new Animation(spriteSheet, duration);
        } catch (SlickException e) {
            e.printStackTrace();
        }
        return animation;
    }
}
