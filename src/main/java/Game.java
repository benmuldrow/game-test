import Util.Player;
import Util.ResourceManager;
import org.newdawn.slick.*;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.particles.effects.FireEmitter;

/**
 * Created by benjaminmuldrow on 5/15/16.
 */
public class Game extends BasicGame {

    public Game(String title) {
        super(title);
    }

    private Circle player;

    public void init(GameContainer gameContainer) throws SlickException {
        player = new Circle(32,32,8);
        gameContainer.setVSync(true);

        // TODO sprite sheet, map

        // TODO collision map

        // TODO render layout values (in tiles)

        // TODO identify player sprite
    }

    public void update(GameContainer gameContainer, int delta) throws SlickException {
        // TODO check user input
        Input input = gameContainer.getInput();
        if (input.isKeyDown(Input.KEY_LEFT)) {
            player.setCenterX(player.getCenterX() - delta * 0.1f);
        }
        if (input.isKeyDown(Input.KEY_RIGHT)) {
            player.setCenterX(player.getCenterX() + delta * 0.1f);
        }
        if (input.isKeyDown(Input.KEY_UP)) {
            player.setCenterY(player.getCenterY() - delta * 0.1f);
        }
        if (input.isKeyDown(Input.KEY_DOWN)) {
            player.setCenterY(player.getCenterY() + delta * 0.1f);
        }
    }

    public void render(GameContainer gameContainer, Graphics graphics) throws SlickException {
        // TODO draw updates
        graphics.setBackground(Color.lightGray);
        graphics.scale(4,4);
        graphics.fill(player);
        graphics.draw(player);
    }
}
